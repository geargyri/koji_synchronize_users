#!/bin/bash/python3

import ldap
import re
import sys
import koji


"""
	Main method for comparing CERN users within 'koji-users' egroup
    against CERN users that are added in koji with build rights.
"""
def main():

    ## Main variables
    LDAPHOST = "xldap.cern.ch"
    KOJI_SERVER = "https://kojihub.cern.ch/kojihub"
    EGROUP = "koji-users"

    ## Connect to LDAP to retieve EGROUP users
    conn = ldap.initialize(f"ldap://{LDAPHOST}")
    egroup_users = cern_egroup_get_users(EGROUP, conn, [], True)
    conn.unbind()
    print(type(egroup_users))
    print(len(egroup_users))

    ## Connect to koji to retieve users registered as builders.
    session = koji.ClientSession(KOJI_SERVER)
    koji_users = session.listUsers()
    print(type(koji_users))
    print(len(koji_users))

    ## Compare CERN users registered in egroup 'koji-users' VS CERN users added in koji with build rights.
    egroup_koji_compare(egroup_users, koji_users)


"""
    Arguments:
    egroup -- The egroup of interest.
    conn -- An initialized LDAP connection object
    processed -- A list of already processed egroups. Pass [].
    recursion -- Boolean to enable recursion
    fmt -- Format string to print usernames. Must have '%s'

    Returns:
    A set of formatted usernames (empty if all egroups are empty)
"""
def cern_egroup_get_users(egroup, conn, processed, recursion=False, fmt="%s"):

    base = 'OU=e-groups,OU=Workgroups,DC=cern,DC=ch'
    user_regexp = r'CN=(\S+),OU=Users,OU=Organic Units,DC=cern,DC=ch'
    egroup_regexp = r'CN=(\S+),OU=e-groups,OU=Workgroups,DC=cern,DC=ch'

    users = set()
    egroup_filter = "(&(objectClass=group)(CN=%s))" % egroup
    ldap_results = conn.search_s(base, ldap.SCOPE_SUBTREE, egroup_filter, ['member'])
    for dn, entry in ldap_results:
        if 'member' in entry:
            for result in entry['member']:
                match = re.match(user_regexp, result.decode('utf-8'))
                if match is not None:
                    users.add(fmt % match.group(1))
                    continue
                if recursion == True:
                    match = re.match(egroup_regexp, result.decode('utf-8'))
                    if match is not None:
                        sys.stderr.write("Found egroup %s in %s\n" % (match.group(1), egroup))
                        expanded_egroup = cern_egroup_get_users([match.group(1)], conn, processed, recursion, fmt)
                        users = users.union(expanded_egroup)
    return users


"""
    koji-users: CERN users registered in CERN e-group 'koji-users'
    koji-builders: CERN users added in koji with build rights.
    egroup_koji_compare()
"""
def egroup_koji_compare(egroup_users, koji_users):

    ## Comparison based on egroup koji-users
    egroup_users_active_builders = []
    egroup_users_to_activate = []
    egroup_users_not_builders = []
    for egroup_user in egroup_users:
        koji_user_is_builder = False
        for koji_user in koji_users:
            if  koji_user["name"] == egroup_user:
                koji_user_is_builder = True
                if koji_user["status"] == 0:
                    koji_user["egroup_name"] = egroup_user
                    egroup_users_active_builders.append(koji_user)
                else:
                    koji_user["egroup_name"] = egroup_user
                    egroup_users_to_activate.append(koji_user)
                break
        if not koji_user_is_builder:
            egroup_users_not_builders.append(egroup_user)

    print(f"\tkoji-users in koji and activated\t= {len(egroup_users_active_builders)}")

    print(f"\n\tkoji-users in koji, NOT activated = {len(egroup_users_to_activate)}")
    for item in egroup_users_to_activate:
        print(f"koji-user {item['egroup_name']} exists as koji-builder, but status is not 0. Needs to be activated.\n\tCheck: {item}")

    print(f"\n\tkoji-users NOT in koji = {len(egroup_users_not_builders)}")
    for eu in egroup_users_not_builders:
        print(f"koji-user {eu} does NOT exist as koji-builder. To be added as active koji-builder.")


    ## Comparison based on koji builders
    active_koji_users_in_egroup = []
    koji_users_to_deactivate = []
    for koji_user in koji_users:
        if koji_user["status"] == 0:
            if koji_user["name"] in egroup_users:
                active_koji_users_in_egroup.append(koji_user)
            else:
                koji_users_to_deactivate.append(koji_user)

    print(f"\n\tActive koji-builders registered in egroup\t\t= {len(active_koji_users_in_egroup)}.\nThese users are included in koji are registered in egroup 'koji-users'!")
    print(f"\n\tkoji-builders to deactivate\t= {len(koji_users_to_deactivate)}.\nThese users in koji installation are not in 'koji-users' egroup and should be de-activated!")


"""
    change_koji_status() changes the status of a given list of koji builders.
"""
def change_koji_status(koji_users_list, status):

    print(f"The status of {len(koji_users_list)} koji builders will change to {status}")
    # TODO


"""
    add_as_builder() adds as koji builders all the users of a given list.
"""
def add_as_builder(egroup_users_list):

    print(f"{len(egroup_users_list)} koji-users will be added to koji with build rights.")
    # TODO



if __name__ == "__main__":

    main()
