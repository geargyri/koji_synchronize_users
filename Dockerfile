FROM gitlab-registry.cern.ch/linuxsupport/cs9-base:latest

MAINTAINER Georgios Argyriou <geargyri@cern.ch>

COPY *repo /etc/yum.repos.d/

RUN dnf install -y python3-ldap \
    python3-koji


CMD /bin/bash